#include "FastLED.h"

/*****************************************************************************/

// Number of RGB LEDs in strand:
#define NUM_LEDS 32

#define LEFT_OF_CENTER 15
#define RIGHT_OF_CENTER 17

// Chose 2 pins for output; can be any valid output pins:
#define DATA_PIN 7
#define CLOCK_PIN 3

#define MIN_STEP 1
#define MAX_STEP 10

// Define the array of leds
CRGB leds[NUM_LEDS];

#define CONCURRENT 8

// You can optionally use hardware SPI for faster writes, just leave out
// the data and clock pin parameters.  But this does limit use to very
// specific pins on the Arduino.  For "classic" Arduinos (Uno, Duemilanove,
// etc.), data = pin 11, clock = pin 13.  For Arduino Mega, data = pin 51,
// clock = pin 52.  For 32u4 Breakout Board+ and Teensy, data = pin B2,
// clock = pin B1.  For Leonardo, this can ONLY be done on the ICSP pins.
//LPD8806 strip = LPD8806(nLEDs);


class Pixel {
  public:
  int indexNum;
  float colorRatio[3]; // red, green, blue
  int colorValue[3]; // red, green, blue
  int setValue[3];
  int location;
  int stepping;
  int steps;
  String pixelMode;

  void nextStep(String pixelMode) {
    if ( pixelMode == "Chaser" ) {
      nextChaserStep();
    }
    else if ( pixelMode == "Breather" ) {
      nextBreatherStep();
    }
    else if ( pixelMode == "RunningLight" ) {
      nextRunningLightStep();
    }
    else if ( pixelMode == "Rainbow" ) {
      nextRainbowStep();
    }
    else if ( pixelMode == "ChaseTheRainbow" ) {
      nextChaseTheRainbowStep();
    }
  };

  void setLED(String pixelMode) {
    if ( pixelMode == "Chaser" ) {
      // Nothing to do
    }
    else if ( pixelMode == "Breather" ) {
      setBreather();
    }
    else if ( pixelMode == "RunningLight" ) {
      setRunningLight();
    }
    else if ( pixelMode == "Rainbow" ) {
      setRainbow();
    }
    else if ( pixelMode == "ChaseTheRainbow" ) {
      setChaseTheRainbow();
    }
  };

  void reset(String pixelMode, int index) {
    if ( pixelMode == "Chaser" ) {
      resetChaser(index);
    }
    else if ( pixelMode == "Breather" ) {
      resetBreather(index);
    }
    else if ( pixelMode == "RunningLight" ) {
      resetRunningLight(index);
    }
    else if ( pixelMode == "Rainbow" ) {
      resetRainbow(index); // not sure there's anything to do here?
    }
    else if ( pixelMode == "ChaseTheRainbow" ) {
      resetRainbow(index); // not sure there's anything to do here?
    }
  }; 

  private:
  //// Chaser stuff ////
  boolean resetChaser(int index) {
    indexNum = index;
    if ( indexNum < CONCURRENT ) {
      /* colorRatio[0] = 1;   // red
      colorRatio[1] = 0;   // green
      colorRatio[2] = 1;   // blue
      colorValue[0] = 255; // red
      colorValue[1] = 0; // green
      colorValue[2] = 255; // blue */
      
      colorRatio[0] = random(0,3) * .5;   // red
      colorRatio[1] = random(0,3) * .5;   // green
      colorRatio[2] = random(0,3) * .5;   // blue
      colorValue[0] = 255; // red
      colorValue[1] = 255; // green
      colorValue[2] = 255; // blue
      location = random(LEFT_OF_CENTER,RIGHT_OF_CENTER);
      stepping = random(MIN_STEP,MAX_STEP);
      steps = stepping;
      return true;
    } else {
     return false;
    }
  };
  
  void nextChaserStep() { // TODO: Pull the leds[location]s out. They really should be in setChaser
    leds[location] = CRGB(0, 0, 0);
    if ( steps <= 0 ) {
      if ( location > LEFT_OF_CENTER ) {
        location++;
      } else {
        location--;
      }
      steps = stepping;
    } else {
      steps--;
    }
    if ( ( 0 > location ) || ( location > NUM_LEDS ) ) {
      resetChaser(indexNum);
    }
    for ( int i = 0; i < 3; i++ ) {
      setValue[i] = ( colorValue[i] * colorRatio[i] );
    }
    leds[location] = CRGB( setValue[0], setValue[1], setValue[2] );
  };

  //// Breather stuff ////
  boolean resetBreather(int index) {
    indexNum = index;
    if ( indexNum < CONCURRENT ) {
      int color = random(1,8);
      colorRatio[0] = bitRead(color, 0);   // red
      colorRatio[1] = bitRead(color, 1);   // green
      colorRatio[2] = bitRead(color, 2);   // blue
      colorValue[0] = 0;
      colorValue[1] = 0;
      colorValue[2] = 0;
      stepping = random(MIN_STEP,MAX_STEP);
      location = random(0,NUM_LEDS);
      return true;
    } else {
     return false;
    }
  };
  
  void nextBreatherStep() {
    int resetMe = 0;
    for ( int i = 0; i < 3; i++ ) {
      colorValue[i] = ( colorValue[i] + ( stepping * colorRatio[i] ) ); // Increase by stepping
      if ( ( colorValue[i] > ( 255 * colorRatio[i] ) ) &&
         ( ( colorValue[i] - ( 255 * colorRatio[i] ) ) < ( stepping * colorRatio[i] ) ) ) {
        colorValue[i] = ( 255 * colorRatio[i] ); // make sure to hit full brightness
      }
      if ( colorValue[i] > ( 511 * colorRatio[i] ) ) { // you've gone too far!
        leds[location] = CRGB(0,0,0);
        resetMe++;
      }
    }
    if ( resetMe > 0 ) {
      resetBreather(indexNum);
    }
  };

  void setBreather() {
    for ( int i = 0; i < 3; i++ ) {
      if (colorValue[i] > ( 255 * colorRatio[i] ) ) {
        setValue[i] = ( 511 * colorRatio[i] ) - colorValue[i];
      } else {
        setValue[i] = colorValue[i];
      }
    }
    leds[location] = CRGB(setValue[0], setValue[1], setValue[2]);
  };

  //// RunningLight stuff ////
  boolean resetRunningLight(int index) {
    indexNum = index;
    if ( indexNum < 3 ) {
      if ( index == 0 ) { // Headlights
        colorValue[0] = 0;
        colorValue[1] = 0;
        colorValue[2] = 0;
        colorRatio[0] = 1;
        colorRatio[1] = 1;
        colorRatio[2] = 1;
        location = 15;
        stepping = 5;
      } else if ( indexNum == 1 ) { // Taillights
        colorValue[0] = 0;
        colorValue[1] = 0;
        colorValue[2] = 0;
        colorRatio[0] = 1;
        colorRatio[1] = 0;
        colorRatio[2] = 0;
        location = 0;
        stepping =6;
      } else if ( indexNum == 2 ) { // Sidelights
        colorValue[0] = 255;
        colorValue[1] = 255;
        colorValue[2] = 0;
        colorRatio[0] = 1;
        colorRatio[1] = .75;
        colorRatio[2] = 0;
        location = 14;
        stepping = 3;
      }
      return true;
    } else {
      return false;
    }
  };
  
  void nextRunningLightStep() {
    int resetMe = 0;
    if ( ( indexNum == 0 ) || ( indexNum == 1 ) ) { // headlights && taillights
      for ( int i = 0; i < 3; i++ ) {
        colorValue[i] = ( colorValue[i] + ( stepping * colorRatio[i] ) );
        if ( ( colorValue[i] > ( 255 * colorRatio[i] ) ) &&
           ( ( colorValue[i] - ( 255 * colorRatio[i] ) ) < ( stepping * colorRatio[i] ) ) ) {
          colorValue[i] = ( 255 * colorRatio[i] );
        }
        if ( colorValue[i] > ( 511 * colorRatio[i] ) ) {
          leds[location] = CRGB(0,0,0);
          leds[getMirrorLocation(location)] = CRGB(0, 0, 0);
          resetMe++;
        }
      }
      if ( resetMe > 0 ) {
        resetRunningLight(indexNum);
      }
    } else if ( indexNum == 2 ) { // side lights
      leds[location] = CRGB(0, 0, 0);
      leds[getMirrorLocation(location)] = CRGB(0, 0, 0);
      if ( steps <= 0 ) {
        location--;
        steps = stepping;
      } else {
        steps--;
      }
      if ( 1 > location ) {
        resetRunningLight(indexNum);
      }
      for ( int i = 0; i < 3; i++ ) {
        setValue[i] = ( colorValue[i] * colorRatio[i] );
      }
      leds[location] = CRGB( setValue[0], setValue[1], setValue[2] );
      leds[getMirrorLocation(location)] = CRGB( setValue[0], setValue[1], setValue[2] );
    }
  };

  void setRunningLight() {
    if ( ( indexNum == 0 ) || ( indexNum == 1 ) ) {
      for ( int i = 0; i < 3; i++ ) {
        if (colorValue[i] > ( 255 * colorRatio[i] ) ) {
          setValue[i] = ( 511 * colorRatio[i] ) - colorValue[i];
        } else {
          setValue[i] = colorValue[i];
        }
      }
      leds[location] = CRGB(setValue[0], setValue[1], setValue[2]);
      leds[getMirrorLocation(location)] = CRGB(setValue[0], setValue[1], setValue[2]);
    }
  };

  int getMirrorLocation(int currLocation) {
    return NUM_LEDS - 1 - location;
  };

  //// Rainbow stuff ////
  boolean resetRainbow(int index) {
    indexNum = index;
    if ( indexNum < 1 ) {
      colorValue[0] = 0; // red
      stepping = 5;
      steps = stepping;
      return true;
    } else {
     return false;
    }
  };

  void nextRainbowStep() {
    if ( colorValue[0] < 1536 ) { // we're just going to use one value and figure it out {
      colorValue[0] = colorValue[0] + 1;
    } else {
      colorValue[0] = 0;
    }
  };
  
  void setRainbow() {
    if ( colorValue[0] <= 255 ) {
      setValue[0] = 255;
      setValue[1] = colorValue[0]; // counting up
      setValue[2] = 0;
    } else if ( colorValue[0] <= 511 ) {
      setValue[0] = 511 - colorValue[0]; //counting down
      setValue[1] = 255;
      setValue[2] = 0;
    } else if ( colorValue[0] <= 767 ) {
      setValue[0] = 0;
      setValue[1] = 255;
      setValue[2] = colorValue[0] - 512; //counting up
    } else if ( colorValue[0] <= 1023 ) {
      setValue[0] = 0;
      setValue[1] = 1023 - colorValue[0]; // counting down
      setValue[2] = 255;
    } else if ( colorValue[0] <= 1279 ) {
      setValue[0] = colorValue[0] - 1024; // counting up
      setValue[1] = 0;
      setValue[2] = 255;
    } else if ( colorValue[0] <= 1535 ) {
      setValue[0] = 255;
      setValue[1] = 0;
      setValue[2] = 1535 - colorValue[0];
    }
    for ( int i=0; i<NUM_LEDS; i++ ) {
      leds[i] = CRGB( setValue[0], setValue[1], setValue[2] );
    }
  };

  // Chase The Rainbow //
  boolean resetChaseTheRainbow(int index) {
    indexNum = index;
    if ( indexNum < 1 ) {
      colorValue[0] = 0; // red
      stepping = 1;
      steps = stepping;
      return true;
    } else {
     return false;
    }
  };

  void nextChaseTheRainbowStep() {
    if ( colorValue[0] < 255 ) { 
      colorValue[0] = colorValue[0] + 1;
    } else {
      colorValue[0] = 0;
    }
  };
  
  void setChaseTheRainbow() {
    for ( int i=0; i<NUM_LEDS /2; i++ ) {
      leds[i + (NUM_LEDS /2)] = CRGB( Wheel( (colorValue[0] - ( ( 255 / (NUM_LEDS/2) ) * i ) ) %255 ));
      leds[(NUM_LEDS /2) - i -1] = CRGB( Wheel( (colorValue[0] - ( ( 255 / (NUM_LEDS/2) ) * i ) ) %255 ));
    }
  };

  uint32_t combineBytes( byte byte0, byte byte1, byte byte2 ) {
    unsigned long value = byte0;
    value = value * 256 + byte1;
    value = value * 256 + byte2;
    return value;
  };

  // Input a value 0 to 255 to get a color value.
  // The colours are a transition r - g - b - back to r.
  uint32_t Wheel(byte WheelPos) {
    WheelPos = 255 - WheelPos;
    if(WheelPos < 85) {
      return combineBytes(255 - WheelPos * 3, 0, WheelPos * 3);
      //return ( ( 255 - WheelPos * 3 ) *65535 + 0*255 + WheelPos * 3 );
    }
    else if(WheelPos < 170) {
      WheelPos -= 85;
      return combineBytes(0, WheelPos * 3, 255 - WheelPos * 3);
    } 
    else {
      WheelPos -= 170;
      return combineBytes(WheelPos * 3, 255- WheelPos * 3, 0);
    }
  };
  
};


void setup() {
  FastLED.addLeds<LPD8806, DATA_PIN, CLOCK_PIN, GRB>(leds, NUM_LEDS);
  randomSeed(analogRead(0));
}

Pixel arrayOfPixels[CONCURRENT];
int numModes = 4;
String modeNames[] = { "Breather","Chaser","Rainbow","RunningLight","ChaseTheRainbow" };
//String modeNames[] = { "ChaseTheRainbow" };
  
void loop() {
  // put your main code here, to run repeatedly:
  for ( int mode = 0; mode < numModes; mode++ ) {
    for (int pixel = 0; pixel < CONCURRENT; pixel++) {
      arrayOfPixels[pixel].reset(modeNames[mode], pixel);
    }
    for (int reps = 0; reps < 30000; reps++) {
      for (int pixel = 0; pixel < CONCURRENT; pixel++) {
        arrayOfPixels[pixel].setLED(modeNames[mode]);
      }
      FastLED.show();
      for (int pixel = 0; pixel < CONCURRENT; pixel++) {
        arrayOfPixels[pixel].nextStep(modeNames[mode]);
      }
      delay(10);
    }
  }
}






